<?php

    require_once("animal.php");
    require_once("frog.php");
    require_once("ape.php");
    $sheep = new Animal("shaun");
    echo "Name : " . $sheep->name ."<br>"; // "shaun"
    echo "Legs : " . $sheep->legs ."<br>"; // 4
    echo "Cold blooded : " . $sheep->cold_blooded ."<br>"; // "no"
    echo "<br>";

    $kodok = new Frog("buduk");
    echo "Name : " . $kodok->name ."<br>"; // "buduk"
    echo "Legs : " . $kodok->legs ."<br>"; // 4
    echo "Cold blooded : " . $kodok->cold_blooded ."<br>"; // "no"
    echo $kodok->jump(); // "hop hop"
    echo "<br> <br>";

    $kera = new Ape("kera sakti");
    echo "Name : " . $kera->name ."<br>"; // "kera sakti"
    echo "Legs : " . $kera->legs ."<br>"; // 2
    echo "Cold blooded : " . $kera->cold_blooded ."<br>"; // "no"
    echo $kera->yell(); // "Auooo"
    echo "<br> <br>";

?>